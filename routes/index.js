const express = require('express')
const router = express.Router()

/* GET home page. */
router.get('/', (req, res, next)=>{
  res.render('index', null);
});

router.get('/sender-form', (req, res, next)=>{
  res.render('sender-form', null);
});


router.get('/message-list', (req, res, next)=>{
  res.render('message-list', null);
});

router.get('/subscribe-form', (req, res, next)=>{
  res.render('subscribe-form', null);
});

module.exports = router;
