const express = require('express')
const router = express.Router()
const FBMessaging = MDModules.Firebase.messaging



router.post('/send/topic', (req, res, next)=>{
  const params  = req.body
  const topic   = params.topic
  const payload = {
    data : {
      payload:params.payload
    },
    notification:{
      title: params.title,
      body: params.body
    }
  }

  FBMessaging.sendToTopic(topic, payload)
  .then(result=>{
    console.log(result)
    res.redirect('/message-list')
  })
  .catch(err=>next(err))
})

router.get('/send/test', (req, res, next)=>{
  const topic   = 'notice'
  const timestamp = new Date().getTime()
  const payload = {
    data : {
      payload:'테스트데이터'+timestamp
    },
    notification:{
      title: '테스트타이틀-'+timestamp,
      body: '테스트바디-'+timestamp
    }
  }

  FBMessaging.sendToTopic(topic, payload)
  .then(result=>{
    res.status(200).send(result)
  })
  .catch(err=>next(err))
})

module.exports = router
