const express = require('express')
const router = express.Router()
const util   = require('util')
const Firebase = MDModules.Firebase
const FBMessaging = Firebase.messaging
const FBDatabase  = Firebase.database

// router.post('/subscribe', function(req, res, next) {
//   const params = req.body
//   const registrationTokens = params.reg_tokens
//   const topic = params.topic
//   FBMessaging.subscribeToTopic(registrationTokens, topic)
//   .then(
//     result=>{
//       console.log(result)
//     }
//   )
//   .catch(err=>next(err))
// });
//


router.post('/subscribe', function(req, res, next) {
  const params = req.body
  const topic = params.topic
  const registrationTokens = params.reg_tokens

  const processResult = {output:[]}

  const saveSubsTokenInTopic = function(result){
    processResult.output.push(result)
    if(result.is_success){
      const path = util.format('/reg_tokens_to_topic/%s', topic)
      const data = {token:registrationTokens}
      return FBDatabase.pushToPath(path, data)
    }else{
      return null
    }
  }

  const respond = function(result){
    processResult.output.push(result)
    res.status(200).send(processResult)
  }

  FBMessaging.subscribeToTopic(registrationTokens, topic)
  .then(saveSubsTokenInTopic)
  .then(respond)
  .catch(err=>next(err))
});

router.post('/unsubscribe', function(req, res, next) {
  const params = req.body
  const topic = params.topic
  const registrationTokens = params.reg_tokens

  const processResult = {output:[]}

  const removeUnSubsTokenInTopic = function(result){
    processResult.output.push(result)
    if(result.is_success){
      const path = util.format('/reg_tokens_to_topic/%s/%s', topic, registrationTokens)
      return FBDatabase.removeToPath(path)
    }else{
      return null
    }
  }

  const respond = function(result){
    processResult.output.push(result)
    res.status(200).send(processResult)
  }

  FBMessaging.unsubscribeFromTopic(registrationTokens, topic)
  .then(removeUnSubsTokenInTopic)
  .then(respond)
  .catch(err=>next(err))
});

module.exports = router;
