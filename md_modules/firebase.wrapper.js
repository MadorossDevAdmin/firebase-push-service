
const FirebaseAdmin = require('firebase-admin')
const serviceAccount = require('../firebase-account-key.json')

FirebaseAdmin.initializeApp({
  credential: FirebaseAdmin.credential.cert(serviceAccount),
  databaseURL: "https://madorosspushtest.firebaseio.com"
});

const Options = {
  timeToLive: 60
}

module.exports={
  messaging : {
    /** @description FCM 구독할 주제&기기토큰 등록
     * @param {string[]} registrationTokens 주제를 구독할 기기 토큰값들
     * @param {string} topic 구독할 주제
     * @return {object} Promise Object
     */
    subscribeToTopic : function(registrationTokens, topic){
      return new Promise((resolve, reject)=>{
        FirebaseAdmin.messaging().subscribeToTopic(registrationTokens, topic)
        .then(response=>{
          const successCount = response.successCount
          const failureCount = response.failureCount
          const errors = response.errors
          if(failureCount > 0){
            resolve({is_success:0, errors:errors})
          }else{
            resolve({is_success:1, success_count :successCount })
          }
        })
        .catch(err=>reject(err))
      })
    },

    /** @description FCM 구독할 주제&기기토큰 등록해제
     * @param {string[]} registrationTokens 주제를 구독할 기기 토큰값들
     * @param {string} topic 구독할 주제
     * @return {object} Promise Object
     */
    unsubscribeFromTopic : function(registrationTokens, topic){
      return new Promise((resolve, reject)=>{
        FirebaseAdmin.messaging().unsubscribeFromTopic(registrationTokens, topic)
        .then(response=>{
          const successCount = response.successCount
          const failureCount = response.failureCount
          const errors = response.errors
          if(failureCount > 0){
            resolve({is_success:0, errors:errors})
          }else{
            resolve({is_success:1, success_count :successCount })
          }
        })
        .catch(err=>reject(err))
      })
    }

    /** @description 특정그룹으로 푸시발송
     * @param {string} notificationKey 알림키
     * @param {object} payload 전송 payload data
     * @return {object}
     */
    ,sendToDeviceGroup : function(notificationKey, payload){
      return new Promise((resolve, reject)=>{
        FirebaseAdmin.messaging().sendToDeviceGroup(notificationKey, payload, Options)
        .then(response=>resolve(response))
        .catch(err=>reject(err))
      })
    }

    ,sendToDevices : function(registrationTokens, payload){
      return new Promise((resove, reject)=>{
        FirebaseAdmin.messaging().sendToDevice(registrationTokens, payload, options)
        .then(response=>resolve(response))
        .catch(err=>reject(err))
      })
    }

    /** @description 특정주제로 푸시발송
     * @param {string} topic 주제
     * @param {object} payload 전송 payload data
     * @return {object}
     */
    ,sendToTopic : function(topic, payload){
      return new Promise((resolve, reject)=>{
        FirebaseAdmin.messaging().sendToTopic(topic, payload, Options)
        .then(response=>resolve(response))
        .catch(err=>reject(err))
      })
    }
  },
  database : {
    saveToPath:function(path, data){
      return new Promise((resolve, reject)=>{
        FirebaseAdmin.database().ref(path).set(data)
        .then(()=>{resolve({path:path, data:data})})
        .catch(err=>reject(err))
      })
    },
    pushToPath:function(path, data){
      return new Promise((resolve, reject)=>{
        const storeRef = FirebaseAdmin.database().ref(path)
        const newRef   = storeRef.push()
        newRef.set(data)
        .then(()=>{resolve({path:path, data:data})})
        .catch(err=>reject(err))
      })
    },
    readFromPath: function(path){
      return new Promise((resolve, reject)=>{
        FirebaseAdmin.database().ref(path).once('value')
        .then((snapshot)=>{resolve(snapshot)})
        .catch(err=>reject(err))
      })
    },
    removeToPath : function(path){
      return new Promise((resolve, reject)=>{
        FirebaseAdmin.database().ref(path).remove()
        .then(()=>{resolve(path)})
        .catch(err=>reject(err))
      })
    },
    getTimestamp : function(){
      return FirebaseAdmin.database.ServerValue.TIMESTAMP
    }
  }

}
